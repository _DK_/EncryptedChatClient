package org.com1028.eccTest;

import static org.junit.Assert.*;

import org.com1028.ecc.Alert;
import org.junit.Test;

public class AlertTest {

	@Test
	public void testAlert() {
		
		// Attempt to create an Alert object
		Alert a = new Alert("Alert test", "CLOSE");
		
		// Make sure the object is properly created
		assertNotNull(a);
		
		// Check that the close method doesn't throw any exceptions
		a.close();
		
	}

}
