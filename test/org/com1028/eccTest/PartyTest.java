package org.com1028.eccTest;

import static org.junit.Assert.*;

import org.com1028.ecc.Party;
import org.junit.Test;

public class PartyTest {

	@Test
	public void testParty() {
		
		// Create a party object
		Party p = new Party("Name", "IP address");
		
		// Check the object was created successfully
		assertNotNull(p);
		
		// Check to see if the nickname is returned correctly
		assertEquals("Name", p.getNickname());
		
		// Check to see if the IP address
		assertEquals("IP address", p.getIPAddress());
		
	}

}
