package org.com1028.eccTest;

import static org.junit.Assert.*;

import org.com1028.ecc.ChatInterface;
import org.com1028.ecc.Party;
import org.junit.Test;

public class ChatInterfaceTest {

	@Test
	public void testChatInterface() {
		
		// Create a chat Interface object
		ChatInterface ci = ChatInterface.getInstance();
		
		// Make sure that the object was created
		assertNotNull(ci);
		
		// See if it returns the JFrame
		assertNotNull(ci.getJFrame());
		
		// Check that methods execute appropriately
		ci.addToChatHistory("test");
		ci.setPartyDetails(new Party("test", "party"));
		ci.reset();
		
	}

}
