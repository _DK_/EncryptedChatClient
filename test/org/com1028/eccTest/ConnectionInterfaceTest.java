package org.com1028.eccTest;

import static org.junit.Assert.*;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;

import org.com1028.ecc.ConnectionInterface;
import org.com1028.ecc.DB;
import org.com1028.ecc.Party;
import org.junit.Test;

public class ConnectionInterfaceTest {

	@Test
	public void testConnectionInterface() {
		
		String thisIP = "";
		try {
			thisIP = InetAddress.getLocalHost().toString();
		} catch (UnknownHostException e1) {
			e1.printStackTrace();
		}
		Party thisParty = new Party("this", thisIP);
		Party otherParty = new Party("other", thisIP);
		
		// Create a DB object to test fillFields() and getThisParty()
		DB db = DB.getInstance();
		Thread dbThread = new Thread(db);
		dbThread.start();
		while (!DB.getInstance().isReady()) {} // wait for the database to be ready
		
		try {
			// Saving details to the database
			db.saveChatInformation(thisParty, otherParty);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		// Create a chat Interface object
		ConnectionInterface ci = ConnectionInterface.getInstance();
		
		// Make sure that the object was created
		assertNotNull(ci);
		
		// The nickname field should have the same nickname as thisParty
		assertEquals(thisParty.getNickname(), ci.getThisParty().getNickname());

		// Should return an empty string as there was no input
		assertEquals("", ci.getTargetIP()); 
		
		// Test that the class resets correctly, after saving a new nickname
		ci.reset();
		assertEquals(thisParty.getNickname(), ci.getThisParty().getNickname());
		assertEquals("", ci.getTargetIP());
	}

}
