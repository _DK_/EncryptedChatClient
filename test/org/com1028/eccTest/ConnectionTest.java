package org.com1028.eccTest;

import static org.junit.Assert.*;

import org.com1028.ecc.Connection;
import org.com1028.ecc.ConnectionRole;
import org.com1028.ecc.Party;
import org.junit.Test;

public class ConnectionTest {
	
	private Connection c;
	private Party p1, p2, p3;

	@Test
	public void testConnection() {
		setupObjects();
		testSetGetRole();
		testCheckDetails();
	}
	
	private void setupObjects() {
		// Attempt to create a connection object
		c = Connection.getInstance();
		
		// Create party objects to be used in the tests
		p1 = new Party(null, null);
		p2 = new Party("", "");
		p3 = new Party("Name-_ 23 a", "127.0.0.1");
		
		// Check to see if the object was created
		assertNotNull(c);
	}
	
	private void testSetGetRole() {
		// Test that the ConnectionRole can be set and get
		c.setRole(ConnectionRole.CLIENT); // For the role CLIENT:
		assertEquals(ConnectionRole.CLIENT, c.getRole());
		c.setRole(ConnectionRole.SERVER); // For the role SERVER:
		assertEquals(ConnectionRole.SERVER, c.getRole());
	}
	
	private void testCheckDetails() {
		c.setRole(ConnectionRole.CLIENT); // For the role CLIENT:
		c.setTargetIP("127.0.0.1"); // correct IP
		
		c.setThisParty(p1); // Incorrect nickname for p1
		assertFalse(c.checkDetails());
		
		c.setThisParty(p2); // Incorrect nickname for p2
		assertFalse(c.checkDetails());
		
		c.setThisParty(p3); // Correct nickname for p3
		assertTrue(c.checkDetails());
		
		c.setTargetIP("1271.0.0.1"); // incorrect IP
		assertFalse(c.checkDetails());
		
		c.setTargetIP("1.1.1.1.1"); // incorrect IP
		assertFalse(c.checkDetails());
		
		c.setTargetIP(""); // incorrect IP
		assertFalse(c.checkDetails());
		
		c.setTargetIP(null); // incorrect IP
		assertFalse(c.checkDetails());

		c.setRole(ConnectionRole.SERVER); // For the role SERVER:
		c.setTargetIP(null); // As role is a SERVER an invalid targetIP should not matter
		
		c.setThisParty(p1); // Incorrect nickname for p1
		assertFalse(c.checkDetails());
		
		c.setThisParty(p2); // Incorrect nickname for p2
		assertFalse(c.checkDetails());
		
		c.setThisParty(p3); // Correct nickname for p3
		assertTrue(c.checkDetails());
	}

}
