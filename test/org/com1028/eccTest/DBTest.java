package org.com1028.eccTest;

import static org.junit.Assert.*;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.com1028.ecc.DB;
import org.com1028.ecc.Party;
import org.junit.Test;

public class DBTest {

	@Test
	public void testDB() {
		// Create the DB object and run it on a thread
		DB db = DB.getInstance();
		
		// Check the object was created
		assertNotNull(db);
		
		// Start the thread
		Thread dbThread = new Thread(db);
		dbThread.start();
		
		// Give the DB time to setup
		try {
			TimeUnit.SECONDS.sleep(2);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		// DB should be ready now
		assertTrue(db.isReady());
		
		// Test the saveChatInformation and the getPreviousConnection
		Party thisParty = new Party("thisParty", "IP");
		Party otherParty = new Party("otherParty", "IP");
		
		try {
			db.saveChatInformation(thisParty, otherParty); // Saving thisParty and otherParty
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		List<Party> prevCon = db.getPreviousConnectionInfo();
		boolean found = false;
		for (Party p : prevCon) {
			if (p.getNickname().equals(otherParty.getNickname()) && p.getIPAddress().equals(otherParty.getIPAddress())) {
				found = true; // if otherParty was found
			}
		}
		assertTrue(found); // Should contain the otherParty
		assertEquals(thisParty.getNickname(), db.getLastUsedNickname()); // Should be the same as thisParty's nickname
		
		// Check the database closes
		db.closeDatabase();
	}

}
