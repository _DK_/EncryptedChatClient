package org.com1028.eccTest;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ AlertTest.class, ChatClientTest.class, ChatInterfaceTest.class,
		ConnectionInterfaceTest.class, ConnectionTest.class, DBTest.class,
		PartyTest.class })
public class AllTests {
	/**
	 * NOTE - DBTest seems to fail, but when run on its own it will pass.
	 */
}
