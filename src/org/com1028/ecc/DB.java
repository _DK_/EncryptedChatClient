package org.com1028.ecc;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class DB implements Runnable {
	
	private static DB dbInstance;
	private static Thread dbThread;
	private Connection dbConnection;
	private Statement dbStatement;
	private PreparedStatement prepStatement;
	
	private DB() {} // Singleton class - blank constructor to defeat instantiation
	
	/**
	 * Singleton class - method to either create a new DB object, or to just 
	 * return the current DB object if one exists.
	 * @return
	 */
	public static synchronized DB getInstance() {
		if (dbInstance == null) {
			dbInstance = new DB();
			dbThread = new Thread(DB.getInstance());
			dbThread.start();
		}
		return dbInstance;
	}

	/**
	 * The run method for when DB is run on a thread. This will open the database 
	 * and keep the thread running until the database connection is closed
	 */
	public void run() {
		try {
			openDatabase();
			while (dbConnection != null) {
				Thread.sleep(10);
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * A method to be used by the connectionInterface class to tell when 
	 * the database has been opened so it can move on and use it.
	 * @return
	 */
	public boolean isReady() {
		if (dbConnection != null && dbStatement != null) {
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * Get the info of parties that the user has 
	 * successfully connected to previously
	 */
	public List<Party> getPreviousConnectionInfo() {
		List<Party> info = new ArrayList<Party>();
		try {
			ResultSet results = this.dbStatement.executeQuery("SELECT * FROM information");
			while (results.next()) {
				info.add(new Party(results.getString("nickname"), results.getString("IPAddress")));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return info;
	}
	
	/**
	 * Get the most recently used nickname of the user
	 */
	public String getLastUsedNickname() {
		String lastUsedNickname = "";
		try {
			ResultSet results = this.dbStatement.executeQuery("SELECT nickname FROM nickname where id = (SELECT max(id) FROM nickname)");
			if (results.next()) {
				lastUsedNickname = results.getString("nickname");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return lastUsedNickname;
	}
	
	
	/**
	 * Method to save information when a chat session ends. This will 
	 * save the party's most recent nickname as well as updating/adding 
	 * the other party's information.
	 * @param thisParty
	 * @param otherParty
	 * @throws IOException
	 */
	public void saveChatInformation(Party thisParty, Party otherParty) throws IOException {
			try {
				List<Party> data = getPreviousConnectionInfo(); // Get the current info to see if the new info needs to be added or updated
				
				/*
				 * Checking the new party against the previous parties to see if any conflict. Conflicting information will be merged in 
				 * case a party's IP changes when using the same nickname or vise versa.
				 */
				for (Party p : data) { 
					if (p.getNickname() == otherParty.getNickname()) {
						prepStatement = this.dbConnection.prepareStatement("UPDATE information SET IPAddress=? WHERE nickname=?");
						prepStatement.setString(1, otherParty.getIPAddress());
						prepStatement.setString(2, otherParty.getNickname());
					} else if (p.getIPAddress() == otherParty.getIPAddress()) {
						prepStatement = this.dbConnection.prepareStatement("UPDATE information SET nickname=? WHERE IPAddress=?");
						prepStatement.setString(1, otherParty.getNickname());
						prepStatement.setString(2, otherParty.getIPAddress());
					} 
				}
				if (prepStatement == null) { // If there were no conflicts or if data was empty, create a new entry in the database
					prepStatement = this.dbConnection.prepareStatement("INSERT INTO information (nickname, IPAddress) VALUES (?, ?)");
					prepStatement.setString(1, otherParty.getNickname());
					prepStatement.setString(2, otherParty.getIPAddress());
				}
				prepStatement.executeUpdate();
				
				/*
				 * Then update the most recently used nickname for this party.
				 */
				if (getLastUsedNickname() == "") {
					prepStatement = this.dbConnection.prepareStatement("INSERT INTO nickname (nickname) VALUES (?)");
				} else {
					prepStatement = this.dbConnection.prepareStatement("UPDATE nickname SET nickname=? WHERE id=(SELECT max(id) FROM nickname)");
				}
				prepStatement.setString(1, thisParty.getNickname());
				prepStatement.executeUpdate();
			} catch (SQLException e) {
				throw new RuntimeException(e);
			}
	}
	
	private void openDatabase() {
		try {
			if (this.dbConnection == null || this.dbConnection.isClosed()) {
				this.dbConnection = DriverManager.getConnection("jdbc:hsqldb:file:data/filestore;ifexists=true;shutdown=true", "SA", "");
			}
			if (this.dbStatement == null || this.dbStatement.isClosed()) {
				this.dbStatement = this.dbConnection.createStatement();
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}
	
	public void closeDatabase() {
		try {
			if (this.dbStatement != null) {
				this.dbStatement.close();
			}
			if (this.dbConnection != null) {
				this.dbConnection.close();
			}
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
}