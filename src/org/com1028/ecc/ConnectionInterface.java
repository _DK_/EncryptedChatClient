package org.com1028.ecc;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JScrollPane;
import javax.swing.JButton;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.List;

import javax.swing.JTextPane;

@SuppressWarnings("serial")
public class ConnectionInterface extends JFrame {
	
	private static ConnectionInterface connectionInstance;
	private JFrame connectionInterface;
	private JTextField fieldNickname;
	private JTextField fieldIPAddress;
	private JButton btnConnect;
	private JButton btnWait;
	private JScrollPane scrollPane;
	private JTextPane previousConnectionsPanel;
	private JLabel labelPreviousConnections;
	private JLabel labelYourIP;
	private JLabel labelThisIP;
	private Connection connection;
	private Thread connectionThread;
	
	private ConnectionInterface() {} // Singleton class - blank constructor to avoid instantiation
	
	/**
	 * Method to either create the interface for the first time, or
	 * to return the current instance of the interface if one exists.
	 * @return
	 */
	public static synchronized ConnectionInterface getInstance() {
		if (connectionInstance == null) {
			connectionInstance = new ConnectionInterface();
			connectionInstance.initialize();
			connectionInstance.fillFields();
		}
		return connectionInstance;
	}
	
	/**
	 * A method to use the getPreviousConnectionInfo method of DB to list the nicknames and IP 
	 * addresses of previous connections in the previousConectionsPanel as well as getting the 
	 * last used nickname of this party.
	 */
	public void fillFields() {
		while (!DB.getInstance().isReady()) {}
		fieldNickname.setText(DB.getInstance().getLastUsedNickname());
		List<Party> previousConnections = DB.getInstance().getPreviousConnectionInfo();
		for (Party p : previousConnections) {
			previousConnectionsPanel.setText(previousConnectionsPanel.getText() + p.getNickname() + " - " + p.getIPAddress() + "\n");
		}
	}
	
	/**
	 * A method to be called when the user clicks the 'connect' (for client) or 
	 * 'wait for connection' (for server). Each button will pass the relevant role to 
	 * the method. The method will then get the Connection instance and start a thread 
	 * running the connection. It will then set the required information for the connection, 
	 * check the input details and finally make the connection if the details are correct.
	 * @param role
	 */
	public void requestConnection(ConnectionRole role) {
		connection = Connection.getInstance();
		connectionThread = new Thread(connection);
		connection.setRole(role);
		connection.setThisParty(getThisParty());
		connection.setTargetIP(fieldIPAddress.getText());
		if (connection.checkDetails()) {
			connectionThread.start();
			connectionInterface.setVisible(false);
		}
	}
	
	/**
	 * Method to return the JFrame so other classes can dictate whether to show or hide the interface.
	 */
	public JFrame getJFrame() {
		return this.connectionInterface;
	}
	
	/**
	 * A method to get this party (the party using the program). This should get the input nickname and IP address.
	 */
	public Party getThisParty() {
		try {
			return new Party(this.fieldNickname.getText(), InetAddress.getLocalHost().toString());
		} catch (UnknownHostException e) {
			e.printStackTrace();
			return new Party(this.fieldNickname.getText(), ""); // If it cant get the InetAddress, just set it to "" - its not a vital requirement
		}
	}
	
	/**
	 * Gets the input IP Address that the party wants to connect to (if acting as client)
	 * @return
	 */
	public String getTargetIP() {
		return this.fieldIPAddress.getText();
	}
	
	/**
	 * Destroys and recreates the interface
	 */
	public void reset() {
		connectionInstance = null;
		getInstance();
	}

	/**
	 * The method in charge of creating the JFrame for the chat interface and filling it with elements
	 */
	private void initialize() {
		connectionInterface = new JFrame();
		connectionInterface.getContentPane().setBackground(Color.BLACK);
		connectionInterface.setTitle("Encrypted Chat Client");
		connectionInterface.setResizable(false);
		connectionInterface.setBackground(Color.BLACK);
		connectionInterface.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		connectionInterface.setBounds(100, 100, 514, 326);
		connectionInterface.getContentPane().setLayout(null);
		
		// The two labels telling the party what their own IP address is.
		labelYourIP = new JLabel("Your IP: ");
		labelYourIP.setBounds(30, 40, 83, 14);
		labelYourIP.setBackground(Color.BLACK);
		labelYourIP.setForeground(Color.WHITE);
		connectionInterface.getContentPane().add(labelYourIP);
		labelThisIP = new JLabel("Error getting IP address!");
		try {
			labelThisIP.setText(InetAddress.getLocalHost().toString());
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
		labelThisIP.setHorizontalAlignment(SwingConstants.CENTER);
		labelThisIP.setBounds(118, 40, 146, 14);
		labelThisIP.setForeground(Color.GREEN);
		connectionInterface.getContentPane().add(labelThisIP);
		
		// Label and input field for the nickname
		JLabel labelNickname = new JLabel("Nickname: ");
		labelNickname.setBounds(30, 70, 86, 48);
		labelNickname.setForeground(Color.WHITE);
		connectionInterface.getContentPane().add(labelNickname);
		fieldNickname = new JTextField();
		fieldNickname.setBounds(116, 84, 146, 20);
		fieldNickname.setForeground(Color.GREEN);
		fieldNickname.setBackground(Color.DARK_GRAY);
		connectionInterface.getContentPane().add(fieldNickname);
		fieldNickname.setColumns(10);
		
		// Area for the previous connection info
		scrollPane = new JScrollPane();
		scrollPane.setBounds(284, 27, 196, 232);
		scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		connectionInterface.getContentPane().add(scrollPane);
		previousConnectionsPanel = new JTextPane();
		previousConnectionsPanel.setEditable(false);
		previousConnectionsPanel.setBackground(Color.DARK_GRAY);
		previousConnectionsPanel.setForeground(Color.GREEN);
		scrollPane.setViewportView(previousConnectionsPanel);
		labelPreviousConnections = new JLabel("Previous connections");
		labelPreviousConnections.setBackground(Color.DARK_GRAY);
		labelPreviousConnections.setHorizontalAlignment(SwingConstants.CENTER);
		scrollPane.setColumnHeaderView(labelPreviousConnections);
		
		// Label and input field for the IP address
		JLabel labelIPAddress = new JLabel("IPAddress: ");
		labelIPAddress.setBounds(30, 136, 94, 14);
		labelIPAddress.setForeground(Color.WHITE);
		connectionInterface.getContentPane().add(labelIPAddress);
		fieldIPAddress = new JTextField();
		fieldIPAddress.setBounds(116, 133, 146, 20);
		fieldIPAddress.setForeground(Color.GREEN);
		fieldIPAddress.setBackground(Color.DARK_GRAY);
		connectionInterface.getContentPane().add(fieldIPAddress);
		fieldIPAddress.setColumns(10);
		
		// Connect button - for acting as client
		btnConnect = new JButton("Connect to party");
		btnConnect.setBounds(116, 179, 146, 23);
		btnConnect.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				requestConnection(ConnectionRole.CLIENT);
			}
		});
		connectionInterface.getContentPane().add(btnConnect);
		
		// Wait for connection button - for acting as server
		btnWait = new JButton("Wait for connection");
		btnWait.setBounds(116, 228, 146, 23);
		btnWait.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				requestConnection(ConnectionRole.SERVER);
			}
		});
		connectionInterface.getContentPane().add(btnWait);
	}

}
