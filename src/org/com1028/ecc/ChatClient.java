package org.com1028.ecc;


public class ChatClient {

	/**
	 * The main method of the program. As the program is event-driven, only 
	 * the creation of the connection interface is necessary
	 * @param args
	 */
	public static void main(String[] args) {
		ConnectionInterface.getInstance().getJFrame().setVisible(true);
	}
	
	/**
	 * A close method to be used when the user is given opportunity to exit the 
	 * program from within the program (through alerts)
	 */
	public static void close() {
		DB.getInstance().closeDatabase();
		Runtime.getRuntime().exit(0);
	}

}
