package org.com1028.ecc;

import java.io.Serializable;

@SuppressWarnings("serial")
public class Party implements Serializable {
	
	private String nickname;
	private String IPAddress;
	
	public Party(String nickname, String IPAddress) {
		super();
		this.nickname = nickname;
		this.IPAddress = IPAddress;
	}
	
	public String getNickname() {
		return this.nickname;
	}
	
	public String getIPAddress() {
		return this.IPAddress;
	}

}
