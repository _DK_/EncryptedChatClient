package org.com1028.ecc;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import java.awt.Window.Type;

public class Alert {

	private JFrame alert;
	private JTextField alertText;
	private JButton btnCancel;

	/*
	 * Constructor for the object. Create the JFrame and its elements, fill out 
	 * the text for the alert and button, then display the alert.
	 */
	public Alert(String text, String cancel) {
		initialize();
		this.alertText.setText(text);
		this.btnCancel.setText(cancel);
		alert.setVisible(true);
	}
	
	
	/** 
	 * Close method to simply close and delete the JFrame when its no longer needed
	 */
	public void close() {
		alert.setVisible(false);
		alert = null;
	}

	/**
	 * Create the alert JFrame and elements
	 */
	private void initialize() {
		alert = new JFrame();
		alert.setType(Type.POPUP);
		alert.setResizable(false);
		alert.setAlwaysOnTop(true);
		alert.getContentPane().setBackground(Color.BLACK);
		alert.setBackground(Color.BLACK);
		alert.setBounds(100, 100, 400, 141);
		alert.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		alert.getContentPane().setLayout(null);
		
		btnCancel = new JButton("CANCEL");
		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				/*
				 * Depending on the text in the button the program will 
				 * either close itself, interrupt a connection attempt, 
				 * or just close the alert window.
				 */
				if (btnCancel.getText().equals("EXIT")) {
					ChatClient.close();
				} else if (btnCancel.getText().equals("CANCEL")) {
					try {
						Connection.getInstance().reset();
						ConnectionInterface.getInstance().reset();
						close();
					} catch (IOException e1) {
						e1.printStackTrace();
					}
				} else if (btnCancel.getText().equals("CLOSE")) {
					close();
				}
			}
		});
		btnCancel.setBounds(160, 78, 75, 23);
		alert.getContentPane().add(btnCancel);
		
		alertText = new JFormattedTextField();
		alertText.setEditable(false);
		alertText.setForeground(Color.GREEN);
		alertText.setHorizontalAlignment(SwingConstants.CENTER);
		alertText.setBackground(Color.DARK_GRAY);
		alertText.setBounds(10, 10, 374, 57);
		alert.getContentPane().add(alertText);
	}
}