package org.com1028.ecc;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import javax.net.ssl.SSLServerSocket;
import javax.net.ssl.SSLServerSocketFactory;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;

public class Connection implements Runnable {

	// Two regular expressions to be used to check the validity of the user's input
	private static final String IP_REGEX = "^([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.([01]?\\d\\d?|2[0-4]\\d|25[0-5])$";
	private static final String NICKNAME_REGEX = "([A-Z+a-z+0-9 -_]{1,50})";
	// Two completely random string that would never occur otherwise in the chat.
	private static final String DISCONNECT_REQUEST = "E_%7xB77.!0!OE8;:F;!O^T+m.|;UY";
	private static final String DISCONNECT_CONFIRMATION = ".3p966.N1RA^%3-731~6c=3_v+|;6H";
	private static Connection connectionInstance;
	private ConnectionRole role;
	private ChatInterface chatInterface;
	private SSLServerSocket serverSocket;
	private SSLSocket socket;
	private ObjectInputStream dataIn;
	private ObjectOutputStream dataOut;
	private Party thisParty;
	private Party otherParty;
	private Alert alert;
	private String targetIP;
	private String message;
	
	private Connection() {} // Singleton class - blank constructor to avoid instantiation

	/**
	 * Method to either create the connection object for the first time, or
	 * to return the current instance of the object if one exists.
	 * @return
	 */
	public static synchronized Connection getInstance() {
		if (connectionInstance == null) {
			connectionInstance = new Connection();
		}
		return connectionInstance;
	}

	/**
	 * The method to be executed when the class is run on a thread. This 
	 * will start by setting up the connection and then moving the program
	 * along to the chat interface. It will then sit and wait for incoming 
	 * messages as long as the socket isn't null. It will finish when the 
	 * socket is set as null in the reset() method.
	 */
	public void run() {
		try {
			setupConnection();
			chatInterface.getJFrame().setVisible(true);
			chatInterface.setPartyDetails(otherParty);
			while (socket != null) {
				message = (String) dataIn.readObject();
				// Check to see if the message is a request to initiate or continue with the disconnect handshake
				if (message.equals(DISCONNECT_REQUEST) || message.equals(DISCONNECT_CONFIRMATION)) {
					disconnect(message);
				} else {
					chatInterface.addToChatHistory(message);
				}
			} 
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Method to check the details of the party before attempting to make a connection. 
	 * If the role is a server, only the nickname needs to be checked as the IP will be ignored.
	 * @return
	 */
	public boolean checkDetails() {
		if (this.role == ConnectionRole.CLIENT && thisParty.getNickname() != null && targetIP != null) {
			if (thisParty.getNickname().matches(NICKNAME_REGEX) && targetIP.matches(IP_REGEX)) {
				return true;
			}
		} else if (this.role == ConnectionRole.SERVER && thisParty.getNickname() != null) {
			if (thisParty.getNickname().matches(NICKNAME_REGEX)) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Method to create the connection. This will set up the properties 
	 * necessary to make the SSL connection, then either establish itself 
	 * as a server or client. If the program is acting as a server then it 
	 * will wait for a client to connect to it before continuing. Once the 
	 * connection has been made both parties will be established.
	 * @throws InterruptedException 
	 * @throws IOException 
	 * @throws ClassNotFoundException 
	 */
	private void setupConnection() throws InterruptedException, IOException, ClassNotFoundException {
		setupProperties();
		if (this.role == ConnectionRole.SERVER) {
			alert = new Alert("Waiting for a connection...", "EXIT");
			setupServer();
			do {
				Thread.sleep(10); // Wait until there is a connection on the socket before moving on...
			} while (socket == null);
		}
		if (this.role == ConnectionRole.CLIENT) {
			alert = new Alert("Connecting to the other party...", "EXIT");
			setupClient();
		}
		alert.close();
		chatInterface = ChatInterface.getInstance();
		establishParties();
	}
	
	/**
	 * Setting system properties required to make the SSL connection for both the 
	 * server and client
	 */
	private void setupProperties() {
		System.setProperty("javax.net.ssl.keyStore", "certificates/keystore.jks");
		System.setProperty("javax.net.ssl.keyStorePassword", "x4g4HHizc5p8xeO");
		System.setProperty("javax.net.ssl.trustStore", "certificates/cacerts.jks");
		System.setProperty("javax.net.ssl.trustStorePassword", "x4g4HHizc5p8xeO");
	}
	/**
	 * Ready the program as a client. Create the SSL server socket and SSL socket, then accept 
	 * incoming connections on the SSL socket.
	 * @throws IOException
	 */
	private void setupServer() throws IOException {
		SSLServerSocketFactory sslSrvFact = (SSLServerSocketFactory) SSLServerSocketFactory.getDefault();
		serverSocket = (SSLServerSocket) sslSrvFact.createServerSocket(8080, 1);
		socket = (SSLSocket) serverSocket.accept();
		setupStreams();
	}
	
	/**
	 * Ready the program as a client. Create the SSL socket with the target IP and 
	 * port, then setup the object streams.
	 * @throws IOException
	 */
	private void setupClient() throws IOException {
		SSLSocketFactory sslFact = (SSLSocketFactory) SSLSocketFactory.getDefault();
		socket = (SSLSocket) sslFact.createSocket(targetIP, 8080);
		setupStreams();
	}
	
	/**
	 * Setting up the data in/out streams for sending/receiving
	 * messages. Also gets the instance of the chat interface so 
	 * the thread is ready to add messages to the chat history
	 * @throws IOException
	 */
	private void setupStreams() throws IOException {
		dataOut = new ObjectOutputStream(socket.getOutputStream());
		dataIn = new ObjectInputStream(socket.getInputStream());
		chatInterface = ChatInterface.getInstance();
	}
	
	
	/**
	 * Establishing parties by sending the party object for this party, 
	 * and receiving a party object for the other party.
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	private void establishParties() throws IOException, ClassNotFoundException {
		dataOut.writeObject(thisParty);
		do {
			otherParty = (Party) dataIn.readObject();
		} while (otherParty == null);
		chatInterface.addToChatHistory("You have connected with: " + otherParty.getNickname() + "\nStart chatting!");
	}
	
	/**
	 * Method to send a message to the other party, as well as 
	 * adding the message to this party's message history.
	 * @param message
	 * @throws IOException
	 */
	public void sendMessage(String message) throws IOException {
		dataOut.writeObject(thisParty.getNickname() + ": " + message);
		chatInterface.addToChatHistory(thisParty.getNickname() + ": " + message);
	}
	
	/**
	 * A method to handle the disconnect 'handshake' by making sure both 
	 * parties know that they should disconnect before acutally disconnecting.
	 * The disconnection is handled by the reset() method.
	 * @param request
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	public void disconnect(String request) throws IOException, ClassNotFoundException {
		if (request == null) {
			dataOut.writeObject(DISCONNECT_REQUEST);
		} else if (request.equals(DISCONNECT_REQUEST)) {
			dataOut.writeObject(DISCONNECT_CONFIRMATION);
		} else if (request.equals(DISCONNECT_CONFIRMATION)) {
			dataOut.writeObject(DISCONNECT_CONFIRMATION);
			reset();
		}
	}
	
	/**
	 * This method will reset the entire program by calling the 
	 * reset methods of the required classes before destroying 
	 * then recreating the instance of the Connection class.
	 * @throws IOException
	 */
	public void reset() throws IOException {
		// Close and reset the chat interface
		chatInterface.getJFrame().setVisible(false);
		chatInterface.reset();
		// Save the information of the chat before clearing fields
		DB.getInstance().saveChatInformation(thisParty, otherParty);
		// Reset and display the connection interface so the program is ready to be reused
		ConnectionInterface.getInstance().reset();
		ConnectionInterface.getInstance().getJFrame().setVisible(true);
		// Display an alert notifying the user that they have been disconnected
		alert = new Alert("The chat has ended.\nYou have been disconnected.", "CLOSE");
		// Close the sockets if they have not already been cleared
		if (serverSocket != null) {
			serverSocket.close();
		}
		if (socket != null) {
			socket.close();
		}
		// Reset all of the class fields
		connectionInstance = null;
		role = null;
		chatInterface = null;
		serverSocket = null;
		socket = null;
		dataIn = null;
		dataOut = null;
		thisParty = null;
		otherParty = null;
		alert = null;
		targetIP = null;
		message = null;
		// Finally call the getInstance class to create a new connection
		getInstance();
	}
	
	public Party getThisParty() {
		return this.thisParty;
	}
	
	public void setThisParty(Party thisParty) {
		this.thisParty = thisParty;
	}
	
	public void setTargetIP(String IP) {
		this.targetIP = IP;
	}
	
	public ConnectionRole getRole() {
		return this.role;
	}
	
	public void setRole(ConnectionRole role) {
		this.role = role;
	}
	
}
