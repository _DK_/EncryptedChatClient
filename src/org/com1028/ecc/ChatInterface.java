package org.com1028.ecc;

import javax.swing.JFrame;
import javax.swing.JTextArea;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;

import java.awt.Color;
import java.io.IOException;

import javax.swing.SwingConstants;

@SuppressWarnings("serial")
public class ChatInterface extends JFrame {

	private static ChatInterface chatInstance;
	private JFrame chatInterface;
	private JTextArea chatHistoryArea;
	private JTextArea sendMessageArea;
	private JLabel chatInfo;
	
	private ChatInterface() {} // Singleton class - blank constructor to avoid instantiation
	
	/**
	 * The 'constructor' of the class. Used to return a chat interface if one 
	 * already exists, or to create one if it doesn't exist. if it doesn't exist
	 * then the initialize() method will be called also to create the interface.
	 * @return
	 */
	public static synchronized ChatInterface getInstance() {
		if (chatInstance == null) {
			chatInstance = new ChatInterface();
			chatInstance.initialize();
		}
		return chatInstance;
	}
	
	/**
	 * A method to add a passed message into the chat history area of the chat interface.
	 * @param message
	 */
	public void addToChatHistory(String message) {
		chatHistoryArea.append(message + "\n");
	}
	
	/**
	 * Simply returns the JFrame of the class so other classes can dictate when the 
	 * interface should be shown or hidden using .setVisible(boolean)
	 * @return
	 */
	public JFrame getJFrame() {
		return this.chatInterface;
	}
	
	/**
	 * A method to put the details of the other party into the label at the top of the interface.
	 * @param party
	 */
	public void setPartyDetails(Party party) {
		chatInfo.setText("Connected with: " + party.getNickname() + ", IP: " + party.getIPAddress());
	}
	
	/*
	 * A method to reset the object by setting itself to null, then re-creating itself using the getInstance() method
	 */
	public void reset() {
		chatInstance = null;
		getInstance();
	}

	/**
	 * The method in charge of creating the JFrame for the chat interface and filling it with elements
	 */
	private void initialize() {
		chatInterface = new JFrame();
		chatInterface.setResizable(false);
		chatInterface.getContentPane().setBackground(Color.BLACK);
		chatInterface.setBackground(Color.BLACK);
		chatInterface.setBounds(100, 100, 601, 600);
		chatInterface.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		chatInterface.getContentPane().setLayout(null);
		
		chatInfo = new JLabel("Error reading other party's detials.");
		chatInfo.setHorizontalAlignment(SwingConstants.CENTER);
		chatInfo.setBounds(30, 34, 520, 14);
		chatInfo.setForeground(Color.GREEN);
		chatInfo.setBackground(Color.BLACK);
		chatInterface.getContentPane().add(chatInfo);
		
		JButton btnDisconnect = new JButton("Disconnect");
		btnDisconnect.setBounds(450, 30, 100, 23);
		// Click listener for the disconnect button. Clicking this will initiate the disconnect handshake in the Connection class
		btnDisconnect.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					Connection.getInstance().disconnect(null); // null passed so the program knows it has to initiate the disconnect handshake
				} catch (ClassNotFoundException e1) {
					e1.printStackTrace();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
		});
		chatInterface.getContentPane().add(btnDisconnect);
		
		chatHistoryArea = new JTextArea();
		chatHistoryArea.setBounds(30, 59, 520, 352);
		chatHistoryArea.setEditable(false);
		chatHistoryArea.setForeground(Color.GREEN);
		chatHistoryArea.setBackground(Color.DARK_GRAY);
		chatHistoryArea.setLineWrap(true);
		chatInterface.getContentPane().add(chatHistoryArea);
		
		sendMessageArea = new JTextArea();
		sendMessageArea.setBounds(30, 430, 430, 112);
		sendMessageArea.setForeground(Color.GREEN);
		sendMessageArea.setBackground(Color.DARK_GRAY);
		sendMessageArea.setLineWrap(true);
		chatInterface.getContentPane().add(sendMessageArea);
		
		JButton btnSend = new JButton("Send");
		btnSend.setBounds(465, 430, 85, 112);
		// Click listener for the send message button. This will ask the Connection class to send the message which will also add it to the chat history
		btnSend.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					Connection.getInstance().sendMessage(sendMessageArea.getText());
					sendMessageArea.setText(""); // removes the old message so its ready to be used for a new message
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
		});
		chatInterface.getContentPane().add(btnSend);
	}

}
